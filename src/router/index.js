import Vue from "vue";
import Router from "vue-router";
import Mano from "@/components/Mano";
import Tup from "@/components/Tup";
import Tok from "@/components/Tok";
Vue.use(Router);
export default new Router({
  routes: [
    {
      path: "/mano",
      component: Mano
    },
    {
      path: "/",
      component: Tup
    },
    {
      path: "/tok",
      component: Tok
    }
  ]
});
